from client.deezerCli import Deezer
from client.spotifyCli import Spotify
from settings import *

class Sync:
    def __init__(self, clients: list, playlists: dict):
        self.clients = clients
        self.playlists = playlists

    def sync_all(self):
        for name in self.playlists:
            self.sync_playlist(name, self.playlists[name])

    def sync_playlist(self, name: str, playlist: dict):
        print("\nSyncing playlist '{}'...".format(name))
        tracks = self.get_tracks(playlist)
        ids = self.get_ids(tracks, playlist)
        self.add_songs(ids, playlist)
        print("\nSync finished !\n")        

    def get_tracks(self, playlist: dict):
        print("· getting tracks lists...")
        tracks = []
        for client in self.clients:
            if playlist[client.name][0] in ["from","both"]:
                client_tracks = client.get_playlist_tracks(playlist[client.name][1])
                print("· {} tracks found on {}".format(len(client_tracks), client.name))
                tracks += client_tracks
        return tracks
    
    def get_ids(self, tracks: list, playlist: dict):
        print("· searching for tracks in clients...")
        errors = 0
        ids = {}
        for client in self.clients:
            if playlist[client.name][0] in ["to","both"]: 
                ids[client.name] = []
                for track in tracks:
                    try:
                        ids[client.name].append(client.find_track(track["title"], track["artist"]))
                    except FileNotFoundError as error:
                        print(error)
                        errors += 1
                # remove duplicates
                ids[client.name] = list(set(ids[client.name]))
        print("Searching done ! {} files not found".format(errors))
        return ids

    def add_songs(self, ids: dict, playlist: dict):
        print("· adding tracks to clients...")
        for client in self.clients:
            if playlist[client.name][0] in ["to","both"]:
                for track in ids[client.name]:
                    client.add_track(playlist[client.name][1], track)

spotify = Spotify(SPOTIFY_ID, SPOTIFY_SECRET)
deezer = Deezer(DEEZER_ID, DEEZER_SECRET)

clients = [spotify, deezer]

sync_class = Sync(clients, playlists)
sync_class.sync_all()