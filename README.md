# Deezer-Spotify

## Quickstart

### install requirements

```
pip install spotipy deezer deezer-oauth-cli
```

### Set up Deezer

1. Visit [Deezer Developers](https://developers.deezer.com/login?redirect=/api)
2. login with your deezer credentials and go to "my apps"
3. create an application with application domain http://localhost:8080 and redirect url http://localhost:8080/oauth/return

### Set up Spotify

1. Visit [Spotify Developers](https://developer.spotify.com/dashboard/)
2. login with your spotify credentials and click the "create an app" button
3. when visualizing your newly created app, click on edit settings
4. input http://localhost:5000/callback in the Redirect URIs section

### Set up settings.py

1. Copy settings.py.example as settings.py
2. Add your tokens values from the previous step
3. Add playlists to the `playlist` dictionary. The id of a playlist can be found in the sharing link or in the playlist url. The playlist name doesn't matter. For each client you can choose between from/to/both to specify on which way to sync each playlist.

### Run

Run the scrip with: `python sync.py`
A browser windows should open asking you ton confirm access for deezer. You have to manually accept it. After that the script should continue automatically and you can close all browser windows.

## Limitation

- Some songs may not be found even if they exist (because of different name/artist in both clients)
- After two syncs you may find some duplicates. 

This appends when you've added the second search result of a song that has some duplicates. On the first sync the song from client A is added to client B. On the second sync the first search result in client A is not the song already added, so it creates a duplicate. 