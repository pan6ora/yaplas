import deezer, json

from .client import Client
import os

class Deezer(Client):

    def __init__(self, client_id: str, client_secret: str):
        self.name = "deezer"
        os.system("deezer-oauth {} {}".format(client_id, client_secret))
        with open('.env') as f:
            access_token = f.read().splitlines()[0].replace('API_TOKEN=', '')
            print(access_token)
        os.remove(".env")
        self.client = deezer.Client(access_token=access_token)

    def get_playlist_tracks(self, playlist_id: str):
        """ return a list of dicts containing title and artist
        of each track """
        result= self.client.get_playlist(playlist_id)
        tracks_list = []
        for track in result.get_tracks():
            artist = track.get_artist().name
            title = track.title
            tracks_list.append({"title":title,"artist":artist})
        return tracks_list

    def add_track(self, playlist_id: str, track_id: str):
        """ add the given track to the given playlist """
        try:
            post = "playlist/{}/tracks".format(playlist_id)
            self.client.request("POST", post, songs=track_id)
        except deezer.exceptions.DeezerErrorResponse:
            pass

    def find_track(self, name: str, artist: str):
        """ return the first result, else except """
        result = self.client.search(query="",track=name,artist=artist)
        try:
            return result[0].id
        except IndexError:
            raise FileNotFoundError("Track not found on Deezer: '{}' by '{}'".format(name,artist))        