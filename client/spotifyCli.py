import spotipy, json
from spotipy.oauth2 import SpotifyOAuth
import os

from .client import Client

class Spotify(Client):

    def __init__(self, client_id: str, client_secret: str):
        self.name = "spotify"
        self.client = spotipy.Spotify(auth_manager=SpotifyOAuth(
            client_id=client_id,
            client_secret=client_secret,
            redirect_uri="http://localhost:5000/callback",
            scope="user-library-modify,playlist-modify-private,playlist-modify-public"
            ))
        #os.remove(".cache")

    def get_playlist_tracks(self, playlist_id: str):
        """ return a list of dicts containing title and artist
        of each track """
        result = self.client.playlist_items(playlist_id=playlist_id,limit=100)
        tracks_list = []
        for track in result['items']:
            artist = track["track"]["artists"][0]["name"]
            title = track["track"]["name"]
            tracks_list.append({"title":title,"artist":artist})
        return tracks_list

    def add_track(self, playlist_id: str, track_id: str):
        """ add the given track to the given playlist """
        # Avoid to have duplicates
        self.client.playlist_remove_all_occurrences_of_items(playlist_id, [track_id,])
        self.client.playlist_add_items(playlist_id=playlist_id,items=[track_id,])

    def find_track(self, name: str, artist: str):
        """ return the first result, else except """
        name = name.replace("'"," ")
        artist = artist.replace("'", " ")
        q = "{} track:{}".format(artist,name)
        result = self.client.search(q=q,limit=1,type="track")
        try:
            return result["tracks"]["items"][0]["id"]
        except IndexError:
            raise FileNotFoundError("Track not found on Spotify: '{}' by '{}'".format(name,artist))