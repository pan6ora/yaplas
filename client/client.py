from abc import ABC,abstractmethod

class Client(ABC):

    @abstractmethod
    def __init__(self):
        self.name = "client"

    @abstractmethod
    def get_playlist_tracks(self, playlist_id: str):
        """ return a list of dicts containing title and artist
        of each track """
        pass

    @abstractmethod
    def add_track(self, playlist_id: str, track_id: str):
        """ add the given track to the given playlist """
        pass

    @abstractmethod
    def find_track(self, name: str, artist: str):
        """ return the first result, else except """
        pass